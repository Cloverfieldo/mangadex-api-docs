---
order: 93
---
# Manga Creation

## Creation

Similar to how the Chapter Upload works, after a Mangas creation with the Manga Create endpoint it is in a "draft" state, needs to be submitted (committed) and get either approved or rejected by Staff.

The purpose of this is to force at least one CoverArt uploaded in the original language for the Manga Draft and to discourage troll entries. You can use the list-drafts endpoint to investigate the status of your submitted Manga Drafts. Rejected Drafts are occasionally cleaned up at an irregular interval. You can edit Drafts at any time, even after they have been submitted.

Because a Manga that is in the draft state is not available through the search, there are slightly different endpoints to list or retrieve Manga Drafts, but outside from that the schema is identical to a Manga that is published.

## Primary cover

By default, when creating a Manga you'll have a `null` value in the primaryCover field. This is expected.

When this field is set to `null`, then we will fetch the most recent volume Cover related to this Manga and assign it as
main Cover.
But if the last Cover could be a spoiler to a new reader of that Manga, we can assign a Cover in this field which will
be used as the main Cover.
