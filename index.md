---
label: Welcome
order: 100
---

# MangaDex API documentation

MangaDex is an ad-free manga reader offering high-quality images!

This document details our API as it is right now. It is in no way a promise to never change it, although we will
endeavour to publicly notify any major change.
The purpose of this document is to provide tutorials on how to use the API for some common use case scenarios. 
If you wish to browse the detailed list of all available endpoints, proceed to the [swagger](/swagger.html) page instead.

## Acceptable use policy

Usage of our services implies acceptance of the following:

- You **MUST** credit us
- You **MUST** credit scanlation groups if you offer the ability to read chapters
- You **CANNOT** run ads or paid services on your website and/or apps

These may change at any time for any and no reason and it is up to you check for updates from time to time.

## Security issues

If you believe you found a security issue in our API, please check
our [security.txt](https://api.mangadex.org/security.txt) to get in touch privately.

## Bugs and questions

### I have a question

You may join [our Discord](https://discord.gg/mangadex)'s `#dev-talk-api` channel to ask questions or for help.

However we're all busy so please read the docs first, then a second time, or try searching in the channel. Then ask away
if you can't figure it out.

### I found a bug

Please read the docs carefully and **triple-check** the request body you're actually sending to us.
If you're sure you found a bug, then congrats and please report it to us so we can fix it!

Every HTTP response from our services has a `X-Request-ID` header whose value is a UUID.
Please log it in your client and provide it to us. If your request has a body, please also provide the JSON body you
sent (you did check it after all, right?).

If you're not sure how do do this, your client could look something similar to this pseudocode:

```
var httpResponse = httpClient.execute(httpRequest);
if (httpResponse.status >= 500) { # feel free to also log it for 4XXs
  logger.error(```
    Request ID: ${httpResponse.headers['X-Request-ID]'}

    Request:
    ${httpRequest.body}

    Response:
    ${httpResponse.body}
  ```);
}
```

N.B.: Notwithstanding questionable implementation
decisions, [HTTP header names are case-insensitive](https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html#sec4.2) so
check that your HTTP library is properly handling that when retrieving the header's value. Ours are *typically*
always lowercase in practice but do not rely on that (or do, but don't complain later).
