---
order: 96
---

# Comments

Entities on MangaDex generally support user comments, through our forums and with a layer of integration with our API.

The **statistics**-tagged endpoints are used to expose the existence (or lack thereof) of a forum thread for this entity,
and of the number of comments so far.

These endpoints support both single and batch lookups.

## Example

Taking for example the manga [Please Bully Me, Miss Villainess!](https://mangadex.org/title/8b34f37a-0181-4f0b-8ce3-01217e9a602c/please-bully-me-miss-villainess).

Its id is `8b34f37a-0181-4f0b-8ce3-01217e9a602c`.

First, we fetch its statistics:

    GET https://api.mangadex.org/statistics/manga/8b34f37a-0181-4f0b-8ce3-01217e9a602c

Then we look for the `$.statistics.$id.comments` property in the response.

### The entity already has a comment thread

Then you receive the thread's id and reply count so far. For example:

```json
{
  "result": "ok",
  "statistics": {
    "8b34f37a-0181-4f0b-8ce3-01217e9a602c": {
      "comments": {
        "threadId": 1069290,
        "repliesCount": 12
      },
      ...
    }
  }
}
```

This means that its comment thread is at https://forums.mangadex.org/threads/1069290 and has 12 replies.

### The entity doesn't have a comment thread yet

Then `comments` will have a value of literal `null`.

```json
{
  "result": "ok",
  "statistics": {
    "8b34f37a-0181-4f0b-8ce3-01217e9a602c": {
      "comments": null,
      ...
    }
  }
}
```

You may open an entity's comment thread within our website, by clicking the comment counter of that entity.
